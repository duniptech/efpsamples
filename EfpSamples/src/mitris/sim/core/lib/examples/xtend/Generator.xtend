package mitris.sim.core.lib.examples.xtend

import mitris.sim.core.modeling.Atomic
import mitris.sim.core.modeling.InPort
import mitris.sim.core.modeling.OutPort

class Generator extends Atomic {
	
	protected val iStart = new InPort<Job>("iStart")
	protected val iStop = new InPort<Job>("iStop")
	protected val oOut = new OutPort<Job>("oOut")
	int jobCounter
	double period
	
	new(String name, double period) {
		super(name)
		addInPort(iStart)
		addInPort(iStop)
		addOutPort(oOut)
		this.period = period
	}
	
	override initialize() {
		jobCounter =1
		holdIn("active", period)
	}
	
	override deltext(double e) { 
		if(phaseIs("passive") && !iStart.empty){
			activate()
		}
	}
	
	override deltint() {
		jobCounter++
		holdIn("active", period)
	}
	
	override lambda() {
		val job = new Job(""+jobCounter)
		oOut.addValue(job)	
	}
	
}