package mitris.sim.core.lib.examples.xtend

import mitris.sim.core.modeling.EntityBase
import org.eclipse.xtend.lib.annotations.Data

@Data
class Result extends EntityBase{
	double thruput
	double avgTaTime
	int lastSolved
	int lastArrived
	
	new(double thruput, double avgTaTime, int lastSolved, int lastArrived){
		super("Result")
		this.thruput = thruput
		this.avgTaTime = avgTaTime
		this.lastSolved = lastSolved
		this.lastArrived = lastArrived
	}
}