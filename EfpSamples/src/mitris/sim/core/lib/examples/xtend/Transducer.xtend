package mitris.sim.core.lib.examples.xtend

import mitris.sim.core.modeling.Atomic
import java.util.logging.Logger
import mitris.sim.core.modeling.InPort
import mitris.sim.core.modeling.OutPort
import java.util.LinkedList

class Transducer extends Atomic{
	
	static val logger = Logger.getLogger(Transducer.name)
    protected val iArrived = new InPort<Job> ("iArrived")
	protected val iSolved = new InPort<Job>("iSolved")
	protected val oOut = new OutPort<Job>("oOut")
	protected val oResult = new OutPort<Result>("oResult")
	
	val jobsArrived = new LinkedList<Job>()
	val jobsSolved = new LinkedList<Job>()
	
	var totalTa = 0.0
	var clock = 0.0
	var observationTime = 1.0;
	Result result
	
	new (String name, double observationTime){
		super(name)
		totalTa = 0
		clock = 0
		addInPort(iArrived)
		addInPort(iSolved)
		addOutPort(oOut)
		addOutPort(oResult)
		this.observationTime = observationTime
	}
	
	override initialize(){
		holdIn("active", observationTime)
	}
	
	override deltext(double e) {
		clock = clock + e
		if(phaseIs("active")){
			var Job job = null
			if (!iArrived.isEmpty()) {
				job = iArrived.getSingleValue()
				logger.fine("Start job " + job.id + " @ t = " + clock)
				job.time = clock
				jobsArrived.add(job)
			}
			if (!iSolved.isEmpty()) {
				job = iSolved.getSingleValue()
				totalTa += (clock - job.time)
				logger.fine("Finish job " + job.id + " @ t = " + clock)
				job.time = clock
				jobsSolved.add(job)
			}
		}
		logger.info("Deltext: "+showState()) 		
	}
	
	override deltint() {
		clock = clock + getSigma()
		var double throughput
		var double avgTaTime
		if(phaseIs("active")){	
			if (!jobsSolved.isEmpty()) {
				avgTaTime = totalTa / jobsSolved.size()
				if (clock > 0.0) {
					throughput = jobsSolved.size() / clock
				} else {
					throughput = 0.0
				}
			} else {
				avgTaTime = 0.0
				throughput = 0.0
			}
			logger.info("End time: " + clock)
			logger.info("Jobs arrived : " + jobsArrived.size())
			logger.info("Jobs solved : " + jobsSolved.size())
			logger.info("Average TA = " + avgTaTime)
			logger.info("Throughput = " + throughput)
			result = new Result(throughput, avgTaTime, jobsSolved.size(), jobsArrived.size())
			holdIn("done", 0);
		} else {
			passivate();
		}
		logger.info("###deltint:  "+showState())
	}
	
	override lambda() {
		if(phaseIs("done")){
			val job = new Job("null")
			oOut.addValue(job)
			oResult.addValue(result)
		//	logger.info(""+result)
		}
	}
	
}