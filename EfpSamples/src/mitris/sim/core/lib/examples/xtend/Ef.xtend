package mitris.sim.core.lib.examples.xtend

import mitris.sim.core.modeling.Coupled
import mitris.sim.core.modeling.InPort
import mitris.sim.core.modeling.OutPort

class Ef extends Coupled{
	
	protected val iStart = new InPort<Job>("iStart")
	protected val iIn = new InPort<Job>("iIn")
	protected val oOut = new OutPort<Job>("oOut")
	protected val oResult = new OutPort<Result>("oResult")
	
	new(String name, double period, double observationTime){
		super(name)
		addInPort(iIn)
		addInPort(iStart)
		addOutPort(oOut)
		addOutPort(oResult)
		
		val Generator generator = new Generator("generator", period)
		addComponent(generator)
		
		val transducer = new Transducer("transducer", observationTime)
		addComponent(transducer)
		
		addCoupling(generator.oOut, this.oOut)
		addCoupling(generator.oOut, transducer.iArrived)
		addCoupling(this.iIn, transducer.iSolved)
		addCoupling(transducer.oOut, generator.iStop)
		addCoupling(this.iStart, generator.iStart)
		addCoupling(transducer.oResult, this.oResult)
	}
}