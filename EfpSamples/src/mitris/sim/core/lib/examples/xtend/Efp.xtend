package mitris.sim.core.lib.examples.xtend

import mitris.sim.core.modeling.Coupled
import mitris.logger.core.MitrisLogger
import java.util.logging.Level
import mitris.sim.core.simulation.Coordinator
import mitris.sim.core.modeling.InPort
import mitris.sim.core.modeling.OutPort

class Efp extends Coupled{
	
	protected val iStart = new InPort<Job>("iStart")
	protected val oResult = new OutPort<Result>("oResult")
	
	new(String name, double genPeriod, double procTime, double obsTime){
		super(name)
		addInPort(iStart)
		addOutPort(oResult)
		
		val ef = new Ef("ef", genPeriod, obsTime)
		addComponent(ef)
		
		val proc = new Processor("processor", procTime)
		addComponent(proc)
		
		addCoupling(this.iStart, ef.iStart)
		addCoupling(ef.oOut, proc.iIn)
		addCoupling(proc.oOut, ef.iIn)
		addCoupling(ef.oResult, this.oResult)
	}
	
	def static void main(String... args){
		MitrisLogger.setup(Level.ALL);
        
        val efp = new Efp("Efp", 1, 3, 100)
		val coordinator = new Coordinator(efp)
		coordinator.initialize()
		coordinator.simulate(Long.MAX_VALUE)
	}
}