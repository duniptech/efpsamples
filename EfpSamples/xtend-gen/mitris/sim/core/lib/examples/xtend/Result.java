package mitris.sim.core.lib.examples.xtend;

import mitris.sim.core.modeling.EntityBase;
import org.eclipse.xtend.lib.annotations.Data;
import org.eclipse.xtext.xbase.lib.Pure;
import org.eclipse.xtext.xbase.lib.util.ToStringBuilder;

@Data
@SuppressWarnings("all")
public class Result extends EntityBase {
  private final double thruput;
  
  private final double avgTaTime;
  
  private final int lastSolved;
  
  private final int lastArrived;
  
  public Result(final double thruput, final double avgTaTime, final int lastSolved, final int lastArrived) {
    super("Result");
    this.thruput = thruput;
    this.avgTaTime = avgTaTime;
    this.lastSolved = lastSolved;
    this.lastArrived = lastArrived;
  }
  
  @Override
  @Pure
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + (int) (Double.doubleToLongBits(this.thruput) ^ (Double.doubleToLongBits(this.thruput) >>> 32));
    result = prime * result + (int) (Double.doubleToLongBits(this.avgTaTime) ^ (Double.doubleToLongBits(this.avgTaTime) >>> 32));
    result = prime * result + this.lastSolved;
    result = prime * result + this.lastArrived;
    return result;
  }
  
  @Override
  @Pure
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    Result other = (Result) obj;
    if (Double.doubleToLongBits(other.thruput) != Double.doubleToLongBits(this.thruput))
      return false; 
    if (Double.doubleToLongBits(other.avgTaTime) != Double.doubleToLongBits(this.avgTaTime))
      return false; 
    if (other.lastSolved != this.lastSolved)
      return false;
    if (other.lastArrived != this.lastArrived)
      return false;
    return true;
  }
  
  @Override
  @Pure
  public String toString() {
    String result = new ToStringBuilder(this)
    	.addAllFields()
    	.toString();
    return result;
  }
  
  @Pure
  public double getThruput() {
    return this.thruput;
  }
  
  @Pure
  public double getAvgTaTime() {
    return this.avgTaTime;
  }
  
  @Pure
  public int getLastSolved() {
    return this.lastSolved;
  }
  
  @Pure
  public int getLastArrived() {
    return this.lastArrived;
  }
}
